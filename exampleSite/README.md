### Example Site 

You can use this example site with the vt-one theme as a starting point.

- Copy the config.toml file to your main site directory
- Copy the content folder to your main site directory

An example of shortcodes, blog/posts and pages can be seen in the example site here: http://wh-hugo-starter.s3-website-us-east-1.amazonaws.com/

